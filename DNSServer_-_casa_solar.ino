#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>

const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
ESP8266WebServer webServer(80);

#define sala 16
#define ventilador 5
#define cozinha 4
#define geladeira 0
#define varanda 14
#define quarto 12
#define banheiro 13
#define chuveiro 15

const char webpage[] PROGMEM = R"=====(
  <!DOCTYPE html>
<html>

<head>

    <style>
        body {
            background-color: white;
            color: White;
            font-size: 16px;
            font: Stoke, Verdana, liberation;
        }



        a {
            text-underline-offset: unset;
            color: #EEEE00;
            text-decoration: none;

        }

        .ender {
            text-align: right;
            font-size: 12px;
            line-height: 140%;
            float: right;
        }

        .imgLogo {
            margin-bottom: 4px;
            padding-bottom: 4px;
        }

        .menu {
            margin: 3px auto;
        }

        .botao {
            font-size: 14px;
            margin: 0px 4px;
            padding: 5px 4px;
            background-color: #FFEE66;
            width: 100px;
            display: inline-block;
        }

        .apres {
            text-align: center;
            font-size: 18px;
        }

        .listaLentes {
            color: #3040BB;
            background-color: #9932cc;
            padding: 10px 10px 10px 50px;
            margin: 30px 10px 10px 10px;
            border: 2px solid #000055;
            line-height: 200%;
            list-style-type: upper-roman;

        }

        .listaLentes a {
            color: #550099;
        }

        ul {
            color: #3040BB;
            line-height: 200%;
            list-style-type: none;
            align-items: center;
        }

        .preco {
            color: #EE0000;
        }

        .tabOculos {
            border: solid;
            border-collapse: collapse;
            background-color: white;
            color: #9932cc;
            width: 96%;
            text-align: center;
            margin: 0 auto;
            font-family: arial, ubuntu;
        }

        .tabOculos a {
            color: #005599;
        }

        .tabOculos td,
        th {
            border: 3px solid white;
            padding: 6px 12px 6px 12px;

        }

        .titulo {
            color: White;
            background-color: #9932cc;
            font-size: 24px;
            text-align: center;
            font-weight: bold;
            flex-direction: row;
        }

        .dimg{
            flex-direction: row;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .img{
            width: 120px;
            height: 120px;
            border-radius: 15px;
            margin-left: 50px;
        }

        .linTH {
            color: #FFFFFF;
            background-color: #9932cc;
            text-align: center;
            vertical-align: middle;
        }

        .colTH {
            color: #FFFFFF;
            background-color: white;
            text-align: center;
        }

        .colTH1 {
            color: #FFFFFF;
            background-color: #9932cc;
            text-align: center;
        }
        

        .btnOn {
            width: 99%;
            height: 75px;
            background-color: rgba(43, 193, 43, 0.637);
            color: white;
            font-weight: bold;
            font-size: 25px;
            border: #550099 10px;
            border-radius: 10px;
        }

        .btnOff {
            width: 99%;
            height: 75px;
            background-color: gray;
            color: white;
            font-size: 25px;
            border: grey 0.5px;
            border-radius: 10px;
        }
    </style>
</head>

<body>
    <table class="tabOculos">
        <tr>
            <th class="titulo" colspan="2">
                <div class="dimg">
                <h1>Casa Solar   </h1> <img class="img" src="
                data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoGBxMSERMREhMWGBYWGRYWGxkYFBkZFhQWFhoaIBshGxgdICslGx8pHR8dKDQjKCwwMT4xHCE3PDcvOy0wMTEBCwsLDw4PHRERHTAoIik0MzAzMDAwNjAwMjMyOTAwMDAwMDEwMDAwMDMwMjIwMDkwMDAyMDAwMDAwMDAwMDAwMP/AABEIAOEA4AMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcBBAUDAgj/xABOEAACAQIEBAMFAgkHCAsBAAABAgADEQQSITEFBhNBIlFhBzJxgZEUUiNCYnKCobGywRUkJWOis8IWM3ODkpPR4jQ1NkNUdKPS0+HwF//EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACwRAAICAQMCBgIBBQEAAAAAAAABAhEDEiExQVEEEyJhcaEygbEUIzORwQX/2gAMAwEAAhEDEQA/ALmiIgCIiAIiIAiIgCIiAIiIBiYMzK+9sAxGWiaefojPnyXtm0y57drXtfS/raWxw1yUbolK3RYMSLezQYgYIfac987dPPfOKVlte+vvZrX7W7WkpkSjpbQap0ZiIkECIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIBicEt9sr5d8PS1PlWq/i/FFHi9SUOwF8cZx5qHo09QSUY9mYbrp+Ku7n0yjxHTcwYFNAi9u+l2J1JNu5OsulSstVHjwXEtTZsLWa7ISKbHepS3S57sF0J75WPZrdmcPi6ZgKgvdN8vvZb3uv5SnxDfYr+MZucLx2cZWIzgA6e7UU+66/kny7HTXQmGuoa6nRiIlSoiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIieGJxCU1L1GVVUXLMQFA9SdoB7RNLhvFqGIBNGrTqZd8jhrfEDabGKxKU1LuwVRuSbCKd0KPWR7jXGgc1Om1lW4eoN/IrTtu19Cw22Hi93Q4vzCaoKoTTp9yfC7j1+4v6/PLqDo4MZirkWVfcW1vgxHbTYdt99t4Yq3kXUe50+HJlGcjKSAoXtTQbKPXufXzAE3OvOb1468s1ZJ0uvNBiabqFOUZiabbim7boR3RvL5aHJPjrz5quGBVhcHQiEgSXhnEBWU3GV1sGW97X2IPdTrY/sIIG9IJRxTo6+K1Rb5KlvfXuGHf1XvYMLEeGUcJ4utbwnw1ALlb7jzU/jL+sX1tMsmNx3RVxo6kREzKiIiAIiIAiIgCIiAIiIAiIgCIiAYkY9o3BqmKwmSiRdHFQqWCh1UMCLnQWvfXTSSLEV1RS7sFVRcsxAAHqTtK75q5u+0XpUrij3J0ap8R2T03PfymuGMnJOPQvBO9iPcnNWwtY11CjwMlmNwc1tSFOoFr7idrGcSeo3Uq1CxG19FT81Rovx38yZxhiCTYXJ8h/+0m5h6diGc3PYfir8PM+v7J3yim9T5NXzZvYdS1mfQDUKe/qw/YPrroN3rzm/aI+0SjVkUdLrx15zftEfaJGkijpdeOvOb9oj7RGkUb9Vwwsf/sEbEHsZ4riSGCsbMDdHGhJHkR7rW3HlftcDW+0T5qVAwsdRJ0k0SzhXNVrJiNf6xR++o2+K6eizo8c5koYbD/aWbOhIVchDdRjewU3t2Jv6GV4cQU943X73cfnf8fr6+eMopVUq2xIOhtqNj5E285m/DxbvoV0Kya8p89UsbVNHptTqWLAFgyuBvYi2ve1vOSuVp7OOHYeliSzsxrWIp5iAliPEAPv2v8r22Msuc+eMYzqPBWaSexmIiZFBERAEREAREQDEinEvaLgqNQ08zuVNmNNQVUjfUkX+V5J66ZlZbkXBFxuL+Uojj3LtfBPkqp4SSEcao4Hke2nY6zo8NjhkbUmaQipcl2cH4tRxVMVaLhl2O4II3BB1B12M4/MHPeFw11VurUGmWmQQp/KbYfAXPpIJylyvisTQZqVVVpO1mU1XXMU08SoDffY+k7tD2YMN69MfCiW/WWEu8WKE2pS/ROmKe7I1xjmTEY1rufCDdUS4pr5fE+p+Vtpr0qBPvG3oN/rJwvs2PfFfSj/zz6//AJuP/En/AHQ/902WfFFVH+C2uK4IfTcKLLp/H4+c+uvPUcHP8qfyf1LC5GfJr/mup7ub5byTn2beWJ/9L/nlpZYRq3zuS5JckT68deSlvZs/bFL86B/+Sebezit2xFM/Gmw/xGR52Pv/ACRriRrrx153qns7xQ92rRPxLr+xTPFuQcaO9E/Cq/8AFBCy4+5OqJx+vHXnRqclY4f90p+FRP4kTWqcrY5d8M/yam37GMup431X+xa7mv1468+avCsUvvYav8qTkfUAiatVKie/TdfzkZf2iSqfBJudeeRNvd+nb5eX7JpDFDzH1mevLUTRtritdyCLHezAjYgjyPcSe8o84rVy0K7AVdlbZavl8H9Nj28hWzVQd55s/rf47ymTCsipkSimX5ErDlX2gtSy0sVmdNhU1NRPzvvj13+MsjBYunWQVKbKyNqGU3BnnZMUsb3MJRaPeZnHp8z4RqvQGIpGpe2XONW2sDsTfte868o01yQ1RmIiQQIiIBiRj2lcM62AqkDxUvwy/oXzf2C36pJ58VqYZSrC4III8wd5MJaZJolOnZWvsa4japXwxPvAVVHqtlf9RT6SzZS/JxOE4slI/i1KlA+t8yj6sFMuidHilU7XUvkXqszEROYzK3xyZeZKJ+8AfrQdf4Sx5XfGf+0eG/NX9yrLDm+biPwi8+nwfUREwKCJxeMcYbD1EzKBSK3LG92a5uq9gwXxBTctqBqJ0MHilqLmQncggghlI3DKdQfj5g94BtREQDEWmZq4riFKmQr1FVjqFJGYj0XcwDNbBU39+mjfnID+0SJe0Tg2Go4CtVp4ekjg0wGSmqsM1RAdQB2Jkop8UoswUVFzHQAnKWPoDa/ynE9qf/Vlf86j/fU5rhb8yK90Wi/UiO8g8p4bF4Pq1lYuXcZhUYaLa2l7efadOv7MMOfcrVl+JRgP7IP65t+ykf0bT9Xq/vmSyXy5ZxyOn1LSk1JlaYr2W1hfpYhG8g6Mn1ILfsmjT5U4rhBU6OqurKwp1QVYMLXyvY5gNiBeWzI77QeL/ZsDUZTZ3/BJ55nBuR6hcx+UmHiMkmounfdBTk3RTeB4dVrOKdKmzuTYBQTY+ZI0Uep0n6AwaMqIrG7BQCfMgan6yMey/hPRwKuR4qx6p/NOiD4ZQD+kZLY8Tl1yrsMkrdGYiJzGYiIgCYmYgFOc9U/s/F+oNLvQrD5Fb/2kMuJZVftpw1q9Cp9+m6f7tr/45ZfC62ejSf7yK31AM6c2+OEv0aT3imbURE5jMrLi+KReYabOQq0lGYnYAUHb+MlnF+KllQoaqpcZ/wADVRipIFw5TwgAknbYeId4twjCVKvHqlZqVQIrVbM1NghC0+kPERY3E72Wm/DFolwDVwoWwYBrGnY5RvoL/Sa52lpXsi+R8L2NjinC6PSLGmpKlGZ2Gap01dS/ja7DwZtb3kjnHwZ6tACoNWQq4/KtlcfW4mxwnHB0VGYdVVGdTo2ZdGOU6lb99pijKJoc1tmFKmturn6qE3yoEFiWA3vmy/pXHuzS4VxlXqMaRBrJ4atK4Lsqmx0++p2OxGndSNfnDFGnjFJ900Ut8Q9TN+or+qfGE5VXDp9vps3WKGpUV2UUwr2eqqEgZddQWP4tiRckSXVEyoVldVdSCrAEEdwZ6SusfzcKau1B2TP4wtlGViDeo+YHpqdD07ZmIuct2vOuEVXfD0XqCztTRnFrWcqC2nbW+khSTdFpY5RSk1s+D2xGKSnbO6rfQZmAufS+85+ORftFKou7I6kg+8q2K3tvYk2vtc+ZmeJUw+IoLYXRark9wtlXKfRib286Y8pzeigxyrTUKEpkuEWwLP8AftptlI7762BEMybOjiKYarRVtRdmsToWQAqSNjY6i/ex3E0Paat+GYj/AFR+lVDN7FVlp1KLubKC4LH3VLLpmOy38z3nnzrhWq4DEU0UszJ4QBckgg6D5S+N1OL90Whyjneyk/0bT9Hq/vmSyRX2ZYOpSwISrTZGFRzlYEGxsQbGSoS2X838lpfkzErT2jVDi+IYbAIdAVzW7NUPiP6NMX/SMspmABJ2Erb2dqcXxPFY5tQuYr6GqbJ9KakfMS+DbVPsvtkw2tlkUqYVQqiwUAAeQGgnpETAoIiIAiIgCIiAV37aaX4PCv5PUX/aUH/DJbye98BhD/U0h9EUSNe2cfzWgf64D606n/Cd7kJ78Owv+jA+hI/hOmX+CPyzR/gjvRETmMzBlcYQM6YemCQWsgOngDUGzEXBFwLkA6XtLHlY1sMxprSscygK1gSRen0iQBqSKjbDuhHacviFbh8oyyXtROcJTVFyroBc6m51JJLE6kk3JJ8yZyOKc0YNsPnDio26IptVV+xF9advvHS3nexiXEEYotTrMzuuWqpqa2B8GgP4SmQTZtRqe5mlhcM1VsiX3AJAvlv6d2PZdyZ6+PwkNOqUtjysn/oZVPy4R3fclfOamtgqOM3NEsjkCwKMcrMB2GZVNuwJm5w3jeExfDhRq4mmhal0an4REdWC5SQG89xp3E9OZsPTocJqpWYoCv4ozEVHe6qNrjMQuttO4ldcKqNWq52uXYBQlJSbhQT7tiW7nbYTiPaXB18dy1QUr0scjKCTfpuxS2xApg52vrmutiO9zJty/wAdwg6WDpVWZgCq5qbqTlBJ3UAaA6WAFrDsJHcDhEBXqUnW5sC+GVdfzmpb/OSng1UU2FJgLPfI4VQSQCSjZQBcAEg21AIOou0KKS2LSnObuTs1OIYgnEVmF7Kop6G18ouLH853H6M8+D8VoXyGuhqVHqn3wS3RIp6+uVV0Ou/lNZapyuV1qM5YA/fZsyg+hqOF+chXMHCVoUFQoVekzB2ZLmorkFeo4Fg67a2DA5lJE87FObnkn0ul+jtweHx5nHHJ03/0sjD80YNnqUzXQGno2ZlVWFtcpJswGxt3E9OVeJU6q1BRYtSU3S4IshZ1yi/YOj2/JKjtKcweHasbUlzW3ItlQebNsg9TLf5I4McNhwGvcgbgg2BZrkHUXd3IB1AKg2IM68WSUuVsaeP8Fi8Mkoztvp7dSQzMRNzzDgc+4/o8PxDg2JTIPO9Q5dPhe/ynO9lOA6WBDka1map+iPCvyst/nOd7ZMWRQw9Bd6lQtbzCLYD6uPpJpwfBijh6NEbU0RP9lQJu/ThXu/4L8Q+TdiImBQREQBERAEREAgntm/6HQ/8AML/d1Z2PZy1+G4f4OPo7Tje2dv5pQH9eP1U6s6vsxa/DKHxqj6VXnS1/YXyaP8ESeIicxmJpLw6mKnUC+K5O5y5iLFgl7BiLjMBexM3YgHKxnL2Hqkl6YuSSbEhSTuSvuknzteeuA4PRo600sRexJLFb75bnwg+QtN+IIpHliKCVFyuqsp7MAQfLQzxo8MoIwZKNNWGxWmoIv6gTciCTwxWFp1VyVEV10OVlDC420M8aHCqFNg9OjSVhexWmqkX31Am7EA0KXCqa1OoFN7swBYlVZr5iq3sCbnX8pvM39MVgKdWxdASNA2ocA72YWI+Rm3EA0aHCaKEMEuQbguzOVPmucnKfhN6IgCImIBWvOH8443hKHan07j4Maj/VAJZQlZcuVBiOYK9Tfp9Uj/V5aX8ZZom+bbTHsi89qRmIiYFBERAEREAREQDxr4dHFnVWA1swBF/nPqlSVRZVCjyAAH0E9IgGIJiRv2j4pqfDq7IbEhUuOyu6q36iR85MY6pJdyUrdHn/AJc0HxAw2HSpXqEkXp5Qgtuc7sAQPMXHledSpxqmj06VQMlSoQEQ2OfzKkEggbnW4001F4D7GqQOIxDd1RQPgzG/7olicR4ctY0mOjUqgqKbd7Mp+qsf1TXLCMJ6S0kk6ObhOb6FTGvgVD9RcwzEDIzILsAb3011Itp8Jtcc5jw+EKLXfKahNrAmwBALNbZRcXPrInwfj7HjD0GoYcMWq02qrTK1mVFYi7Zj90Xmxw3mZcXjMStJKSotNh1agzNVymyixIAp3LHL330JMl4qfG1WHEnQMSveF88s/DKleilNKmH6SlDmamVcgLl8QKi19Ln3Z9/5Y4tuGnHAUQy1MhXI5DKWA0OcZTc+u0r5MvuhoZP4kLrc/hOH08Waf4WqzU1p5vDnQkMc33dL+eoHrNbiPPFfC4bB4msiOMQGdlVSgRLAqFYsfFlPfex2keTPivYaWd3Hc34eljKeCYP1HyjMAMis/ugm99dNgRrJBK84tzCy8WoUjQw7ZmohajUj1kSra4DZtCLn6zsY7meuOILg1oMKf41UozbpmzDZQoOhJPY7SZYntS6WHHsSuJC+Q+damN6iVqaq9NA90vZh3GUk2O3fv2nly5z59rDi9JKpJ6VJ7hai2Fh1b+8TpoPkZDwyTargjSydRNfAOzUqbOLMVUsLWsxAJFu2s2JmVPClhUUllRQTuQoBN/Mie8RAEREAREQBERAEREAREQDE0+L8PTEUalCp7tRSptuPIj1BsR8JuRCdboFYcv8AAsbwzGZ+i1aiwKM1KxJUkEHITcMCBptvqZP1qtWy+B1UEMc4AZiuqgLe4s1iSbbAa3NuhEvPI5u3yWcrK34VwTErxlsU1BhRNSsQ+mzK4By3zakjt3jl7l/EcNxznotVw7qyB6diVUkFSVJvcWsfjceUsiJd55Pp0onWyL85YetXwNamlJsz9MIgC5/C6lixvlGmwv28zYRynwLEjgtTCGg/WNUME8Oq50a+a+XYHvJ1zBxZMLh3rvsg0HdmOiqPUmbeGrrURaiEFXAZSNirC4P0iGWUYqltd/sKTSK4/wAjsRW4XSpFClei9RwrkAOrsTYMDa50I17WNr3kp5VqVRhKVCrh3WpSVUsyjJ4NFbPci1rba76SSRInmlJU+9kOTZW/H+B4mpxeliUou1NXoEuAALIVzEKTfz7fWez8Mxn8ritVSrUw+ZmSzA01BpkL4CQFIa24GuvrLCiT58q4XFE62VhyTwHG4OriVai4Jpsi1AVKXU3uDe5uNrDe1wNZ6c18mdVFqUMOyYliM9NNaDE+8wc2VRfXcHzW8suI8+WrV1Gt3Zp8HoPTw9GnVbNUVEV2+8wUAn11m7MRMW73KGYiIAiIgCIiAIiIAiIgGJpcV4nSw9M1KrWFwo7lmOwUdyfKbsg3tY4dVrUqPSBcozuUXVitgCwUanKSBpr4pfHFSkky0VbolVbiYpr1KqNTQC5YlSEH5WUnKPM7DuZsYLErVppVQ3VwGU+YOx+kqXgvtFxNICnWC10HhIbSpbYjN3/SBPrJdi+cMPhOH4epQUuHXJSQmxAp6NmPbLsbd7fGaT8PONKuSXBomcjnFOc6OHxSYWpTqh3KAEKhUio2VTfNtf0vObxbnOrhPsb10RqddM79MMrUiAhOUEnOBmGmh0Pwkd59bNxjCspBuMKR5G9VrfKTiw2/VxT+hGPctec3i3G6eHBuHd+1OkjVKh8vCo0+JsJHsVzhUw/EVwddabU36YV0VlZTU0XMpZgRm00t5yZWmLi403wyKrkprnziWNxDI+IovRo3IpowtrbUtfUtbvbTYd79LkHjmNoUlH2epWwpJsUXM9MgnNk8xe/hPyPn0/bR/msN/pH/AHZ1vZYf6NpH8qr/AHjTslJf06dLk0b9HBIMBxFKy5kLDzV0ZHX4o4BH0m3IXwznR8XUxQw6otOghdWqAsara5dAwyqbHzOo22ngOf3qcOfGUkpipSdEqI2ZlOcqAVYEWFjfXyI9ZzPDK6rt9melk7iQKvzpihw6nj1WjrUKMhVzcZmAIbPpttY+fpNjivtAFPA0MSlL8JXzBUY+FShs5JGpAO217jaPIl261+xpZ1eZebqWBZFrU6vjzFSgRg2TLf8AHBHvDcTtYWsHRHF7MoYX3swuLyq/abXqvR4fUrFM9SnVeyKVC5hRNtWN7X3/AFST8c5qqYKhg0p0WqNVRNbEgAKtwAPec30FxLvD6I1y7+iXHZUTOJDcDznU/lNuH1aakZiqVFupBCZ/EpJvpcXBHwg88p9vq4V2SklO6B6ik5qgte5uAi72vv5jaZ+TPt0v9EaWTKZmjwnE1KlMtUQIwd1srZlKqxCsGsLhhZtu83pm1RUREQBERAEREAREQDE4fG8FWfE4arRKjpLVLBr2qK/TGS4929iQddVGhndmITolOiJc3cEoYqjUP2Z/tGU5CKZVupbwhqg8BW+92ItecHi/s/r/AMn0KaEPWomqxUGwYVSCQpPdbLva+vpLLiaxzyjSXTcspNFc808CxGOGApU6bp06Z6rOpVaRYUwRr75GU6LftrreZ5q5Vrti6OJopnp0Bhly3/COtNyWKjvYW+NzbaWLElZ5Kkul/YU2iveL8uVsZxVK4RloU+ld3GXP0zmsinxG5Nr2tv6XsETMSkpuSSfQq3ZBPanw+tiUopQpPUKOxayGwBUAamwPynR9nmEelgVw9am6MDUuGUgWZiRZttj5yVEQJLyvQoFtfporTlTgtfh2LqJVw71aLrlWrTTqAZTdCVGq6XuLb23Gs7vPeEq1sC9OlRbM7UiqKvisrgsWI0XTYHy8zYS6JLzNyUnyHK3ZWFXg2IPBaeFFCp1RWLFMhuFzsb322I7zxx/KeJq8MwgWkwq0DWDU2FmK1XLXW+jEAKbA9z3FpasS39RJdOtjWyr+dOF4nFYfAClh6hanTdXUoVKOVpC1ja/unUaTY5qwmOc4LppXNBadJaiU86kMpGcOoIJ8NgDqNDLHiFnapUtr+xr4K0bguIpcZXErhqholsy5FWwU0igvqAmvY20m5zFywMTTrVatJ0xIapkamhYV1Vm6YdQDbw5Rc2NrG5GksCJHnytPsNbI37PeG18Pg0p4jRrsVUm5pobWW4+Zt2vaSSImTlqbbKt27MxESCBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREA//Z
                " alt="">
            </div>
            </th>
        </tr>
        <tr>
            <th class="linTH">
                <h2>Controles</h2>
            </th>
            <!-- <th class="linTH">
                <h2>Dispositivos</h2>
            </th> -->
            <th class="linTH">
                <h2>Gastos simulado</h2>
            </th>
            <!-- <th class="linTH">Preço</th> -->
        </tr>
        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnSala"><button class="btnOn" onclick="liga('a')">Luz da Sala</button></div>
            </th>

            <!-- <td class="alturaMarca">
                <h2>Luz da Sala</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="sala">R$0.00</h2>
            </td>

        </tr>
        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnVentilador"><button class="btnOn" onclick="liga('b')">Ventilador</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Ventilador</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="ventilador">R$0.00</h2>
            </td>

        </tr>
        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnCozinha"><button class="btnOn" onclick="liga('c')">Luz da Cozinha</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Luz da Cozinha</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="cozinha">R$0.00</h2>
            </td>
        </tr>
        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnGeladeira"><button class="btnOn" onclick="liga('d')">Geladeira</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Geladeira</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="geladeira">R$0.00</h2>
            </td>

        </tr>

        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnVaranda"><button class="btnOn" onclick="liga('e')">Luz da Varanda</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Luz da Varanda</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="varanda">R$0.00</h2>
            </td>
        </tr>

        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnQuarto"><button class="btnOn" onclick="liga('f')">Luz do Quarto</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Luz do Quarto</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="quarto">R$0.00</h2>
            </td>
        </tr>

        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnBanheiro"><button class="btnOn" onclick="liga('g')">Luz do Banheiro</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Luz do Banheiro</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="banheiro">R$0.00</h2>
            </td>

        </tr>

        <tr>
            <th class="colTH" rowspan="1">
                <div id="btnChuveiro"><button class="btnOn" onclick="liga('h')">Chuveiro</button></div>
            </th>
            <!-- <td class="alturaMarca">
                <h2>Chuveiro</h2>
            </td> -->
            <td class="alturaMarca">
                <h2 id="chuveiro">R$0.00</h2>
            </td>

        </tr>
        <tr>
            <th class="colTH1" rowspan="1" colspan="1">
                <h1>Gastos totais:</h1></button>
            </th>
            <td class="colTH1">
                <h2 id="total">R$ 0.00</h2>
            </td>

        </tr>
        <tr>
            <th class="colTH" rowspan="1" colspan="2"><button class="btnOn"
                    onclick="resetar()">Resetar</button></th>
        </tr>
    </table>
    <h2 id="demo"></h2>
    <script>
        var tempo = 0;

        var tarifa = 0.000702;

        var luzwh = tarifa * 10;
        var geladeirawh = tarifa * 300;
        var chuveirowh = tarifa * 4600;
        var ventiladorwh = tarifa * 40;

        var ledSala = 0;
        var gastoSala = 0;
        var ventilador = 0;
        var gastoVentilador = 0;
        var ledCozinha = 0;
        var gastoCozinha = 0;
        var geladeira = 0;
        var gastoGeladeira = 0;
        var ledVaranda = 0;
        var gastoVaranda = 0;
        var ledQuarto = 0;
        var gastoQuarto = 0;
        var ledBanheiro = 0;
        var gastoBanheiro = 0;
        var chuveiro = 0;
        var gastoChuveiro = 0;

        var total = 0;

        const myInterval = setInterval(myGreeting, 1000);

        function myGreeting() {
            document.getElementById("demo").innerHTML = `${tempo}`
            tempo += 1

            total = gastoSala + gastoVaranda + gastoVentilador + gastoCozinha + gastoGeladeira + gastoQuarto + gastoBanheiro + gastoChuveiro;



            document.getElementById('total').innerHTML = `R$ ${total.toFixed(2)}`;

            if (ledSala == 1) {
                gastoSala += 2 * luzwh
                document.getElementById("sala").innerHTML = `R$ ${gastoSala.toFixed(2)}`
            }
            if (ventilador == 1) {
                gastoVentilador += ventiladorwh
                document.getElementById("ventilador").innerHTML = `R$ ${gastoVentilador.toFixed(2)}`
            }
            if (ledCozinha == 1) {
                gastoCozinha += luzwh
                document.getElementById("cozinha").innerHTML = `R$ ${gastoCozinha.toFixed(2)}`
            }

            if (geladeira == 1) {
                gastoGeladeira += geladeirawh
                document.getElementById("geladeira").innerHTML = `R$ ${gastoGeladeira.toFixed(2)}`
            }

            if (ledVaranda == 1) {
                gastoVaranda += luzwh
                document.getElementById("varanda").innerHTML = `R$ ${gastoVaranda.toFixed(2)}`
            }

            if (ledQuarto == 1) {
                gastoQuarto += luzwh
                document.getElementById("quarto").innerHTML = `R$ ${gastoQuarto.toFixed(2)}`
            }

            if (ledBanheiro == 1) {
                gastoBanheiro += luzwh
                document.getElementById("banheiro").innerHTML = `R$ ${gastoBanheiro.toFixed(2)}`
            }

            if (chuveiro == 1) {
                gastoChuveiro += chuveirowh
                document.getElementById("chuveiro").innerHTML = `R$ ${gastoChuveiro.toFixed(2)}`
            }

        }

        function liga(a) {
            if (a == 'a') {
                if (ledSala == 1) {
                    ledSala = 0
                    document.getElementById("btnSala").innerHTML = `<button class="btnOn"  onclick="liga('a')">Luz da Sala</button>`;
                    send(1);
                } else {
                    ledSala = 1
                    document.getElementById("btnSala").innerHTML = `<button class="btnOff"  onclick="liga('a')">Luz da Sala</button>`
                    send(0);
                }
            }

            if (a == 'b') {
                if (ventilador == 1) {
                    ventilador = 0
                    document.getElementById("btnVentilador").innerHTML = `<button class="btnOn"  onclick="liga('b')">Ventilador</button>`;
                    send(3);
                } else {
                    ventilador = 1
                    document.getElementById("btnVentilador").innerHTML = `<button class="btnOff"  onclick="liga('b')">Ventilador</button>`
                    send(2);
                }
            }
            if (a == 'c') {
                if (ledCozinha == 1) {
                    ledCozinha = 0
                    document.getElementById("btnCozinha").innerHTML = `<button class="btnOn"  onclick="liga('c')">Luz da Cozinha</button>`;
                    send(5);
                } else {
                    ledCozinha = 1
                    document.getElementById("btnCozinha").innerHTML = `<button class="btnOff"  onclick="liga('c')">Luz da Cozinha</button>`;
                    send(4);
                }
            }

            if (a == 'd') {
                if (geladeira == 1) {
                    geladeira = 0;
                    document.getElementById("btnGeladeira").innerHTML = `<button class="btnOn"  onclick="liga('d')">Geladeira</button>`;
                    send(7);
                } else {
                    geladeira = 1;
                    document.getElementById("btnGeladeira").innerHTML = `<button class="btnOff"  onclick="liga('d')">Geladeira</button>`;
                    send(6);
                }
            }

            if (a == 'e') {
                if (ledVaranda == 1) {
                    ledVaranda = 0;
                    document.getElementById("btnVaranda").innerHTML = `<button class="btnOn"  onclick="liga('e')">Luz da Varanda</button>`;
                    send(9);
                } else {
                    ledVaranda = 1;
                    document.getElementById("btnVaranda").innerHTML = `<button class="btnOff"  onclick="liga('e')">Luz da Varanda</button>`;
                    send(8);
                }
            }

            if (a == 'f') {
                if (ledQuarto == 1) {
                    ledQuarto = 0;
                    document.getElementById("btnQuarto").innerHTML = `<button class="btnOn"  onclick="liga('f')">Luz do Quarto</button>`;
                    send(11);
                } else {
                    ledQuarto = 1;
                    document.getElementById("btnQuarto").innerHTML = `<button class="btnOff"  onclick="liga('f')">Luz do Quarto</button>`;
                    send(10);
                }
            }

            if (a == 'g') {
                if (ledBanheiro == 1) {
                    ledBanheiro = 0;
                    document.getElementById("btnBanheiro").innerHTML = `<button class="btnOn"  onclick="liga('g')">Luz do Banheiro</button>`;
                    send(13);
                } else {
                    ledBanheiro = 1;
                    document.getElementById("btnBanheiro").innerHTML = `<button class="btnOff"  onclick="liga('g')">Luz do Banheiro</button>`;
                    send(12);
                }
            }

            if (a == 'h') {
                if (chuveiro == 1) {
                    chuveiro = 0;
                    document.getElementById("btnChuveiro").innerHTML = `<button class="btnOn"  onclick="liga('h')">Chuveiro</button>`;
                    send(15);
                } else {
                    chuveiro = 1;
                    document.getElementById("btnChuveiro").innerHTML = `<button class="btnOff"  onclick="liga('h')">Chuveiro</button>`;
                    send(14);
                }
            }
        }

        function send(led_sts) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("state").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "led_set?state=" + led_sts, true);
            xhttp.send();
        }

        function resetar() {
          send(20);

         setInterval(zerar(), 1000);

        }

        function zerar(){
          window.location.reload() 
        }
    </script>
</body>

</html>


)=====";

ESP8266WebServer server(80);
int seconds = 0;

void handleRoot()

{

  String s = webpage;

  server.send(200, "text/html", s);
}

void sensor_data()

{
  int a = seconds;

  int temp = a;

  String sensor_value = String(temp);

  server.send(200, "text/plane", sensor_value);
}

void led_control()

{

  String state = "OFF";

  String act_state = server.arg("state");

  if (act_state == "0")

  {

    digitalWrite(sala, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "1")

  {

    digitalWrite(sala, LOW);  //LED OFF

    state = "OFF";
  }

  if (act_state == "2")

  {

    digitalWrite(ventilador, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "3")

  {

    digitalWrite(ventilador, LOW);  //LED OFF

    state = "OFF";
  }

  if (act_state == "4")

  {

    digitalWrite(cozinha, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "5")

  {

    digitalWrite(cozinha, LOW);  //LED OFF

    state = "OFF";
  }

  if (act_state == "6")

  {

    digitalWrite(geladeira, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "7")

  {

    digitalWrite(geladeira, LOW);  //LED OFF

    state = "OFF";
  }

  if (act_state == "8")

  {

    digitalWrite(varanda, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "9")

  {

    digitalWrite(varanda, LOW);  //LED OFF

    state = "OFF";
  }

    if (act_state == "10")

  {

    digitalWrite(quarto, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "11")

  {

    digitalWrite(quarto, LOW);  //LED OFF

    state = "OFF";
  }

    if (act_state == "12")

  {

    digitalWrite(banheiro, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "13")

  {

    digitalWrite(banheiro, LOW);  //LED OFF

    state = "OFF";
  }

    if (act_state == "14")

  {

    digitalWrite(chuveiro, HIGH);  //LED ON

    state = "ON";
  }

  if (act_state == "15")

  {

    digitalWrite(chuveiro, LOW);  //LED OFF

    state = "OFF";
  }

  if (act_state == "20")

  {
    
    digitalWrite(sala, LOW);
    digitalWrite(ventilador, LOW);  //LED OFF
    digitalWrite(cozinha, LOW);
    digitalWrite(geladeira, LOW);
    digitalWrite(varanda, LOW);
    digitalWrite(quarto, LOW);
    digitalWrite(banheiro, LOW);
    digitalWrite(chuveiro, LOW);    
    state = "OFF";
  }
  
  server.send(200, "text/plane", state);
}


void setup(void) {
  pinMode(sala, OUTPUT);

  pinMode(ventilador, OUTPUT);

  pinMode(cozinha, OUTPUT);

  pinMode(varanda, OUTPUT);

  pinMode(quarto, OUTPUT);

  pinMode(geladeira, OUTPUT);

  pinMode(banheiro, OUTPUT);

  pinMode(chuveiro, OUTPUT);

  

  

  Serial.begin(115200);

  Serial.print("IP address: ");

  Serial.println(WiFi.localIP());



  server.on("/", handleRoot);

  server.on("/led_set", led_control);

  server.on("/adcread", sensor_data);

  server.begin();



  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("DNSServer example");

  // modify TTL associated  with the domain name (in seconds)
  // default is 60 seconds
  dnsServer.setTTL(300);
  // set which return code will be used for all other domains (e.g. sending
  // ServerFailure instead of NonExistentDomain will reduce number of queries
  // sent by clients)
  // default is DNSReplyCode::NonExistentDomain
  dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);

  // start DNS server for a specific domain name
  dnsServer.start(DNS_PORT, "www.casasolar.com", apIP);

  // simple HTTP server to see that DNS server is working
  webServer.onNotFound([]() {
    

     String s = webpage;

    server.send(200, "text/html", s);
  });
  webServer.begin();
}

void loop(void) {
  dnsServer.processNextRequest();
  webServer.handleClient();
  server.handleClient();
}
